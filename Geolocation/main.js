var divMap = document.querySelector(".divMap");
var latEl = document.querySelector(".lat");
var lonEl = document.querySelector(".lon");
var bescrijvingEl = document.querySelector(".beschrijving");
var map;
var API_KEY = "AIzaSyBHlzEXk1cGO65POSXz_AYW2DDmKuF6Fv4";
var mapScript = document.createElement('script');
mapScript.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key=' + API_KEY + '&callback=initMap');
mapScript.setAttribute('async', '');
mapScript.setAttribute('defer', '');
document.querySelector('body').appendChild(mapScript);


getLocation();


function update() {

    latEl = document.querySelector(".lat");
    lonEl = document.querySelector(".lon");
    bescrijvingEl = document.querySelector(".beschrijving");

    if (!bescrijvingEl.value == "") {

        let location = {
            "LAT": latEl.value, "LON": lonEl.value, "beschrijving": bescrijvingEl.value
        };

        localStorage.setItem(bescrijvingEl.value, JSON.stringify(location));
    }
    console.log(lonEl.value + "\n" + latEl.value + "\n" + bescrijvingEl.value);
}

function clearStorage() {
    localStorage.clear();
}


function initMap() {
    navigator.geolocation.getCurrentPosition(showMap, handleError);
}

function newMarker(lat, lon) {
    let marker = new google.maps.Marker({
        position: {
            lat: lat,
            lng: lon
        },
        map: map
    });
}

 function setAllMarkers() {

    var archive = {},
        keys = Object.keys(localStorage),
        i = keys.length;


    while (i--) {
        archive[keys[i]] = JSON.parse( localStorage.getItem(keys[i]));

        newMarker(Number(archive[keys[i]].LAT) , Number(archive[keys[i]].LON));
        console.log()
    }

}


function showMap(pos) {
    divMap.style.width = '500px';
    divMap.style.height = "500px";
    map = new google.maps.Map(divMap,
        {
            center:
                {
                    lat: pos.coords.latitude,
                    lng: pos.coords.longitude,
                }, zoom: 14
        });

    let marker = new google.maps.Marker({
        position: {
            lat: pos.coords.latitude,
            lng: pos.coords.longitude
        },
        map: map
    });
    setAllMarkers()
}


var posOptions =
    {
        enableHighAccuracy: false,
        maximumAge: 0,
        timeout: 5000
    };

/**  * Get the current location of the user  */ function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showLocation, handleError, posOptions);
    } else {
        console.log("not supoddroted")
        ;
    }
}

/**  * Show current location coords of the user  * @param position object with position info  */ function showLocation(position) {
    console.log(position.coords.latitude, position.coords.longitude);
    latEl.value = position.coords.latitude;
    lonEl.value = position.coords.longitude;

    update()
}

function handleError(error) {
    console.log(error.code, error.message);
}
