import { Component } from '@angular/core';
import {from} from "rxjs";
import {ActionSheetController} from "@ionic/angular";
import {PhotoService} from "../services/photo.service";
//





@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  ngOnInit() {
    this.photoService.loadSaved();
  }

  constructor(public photoService: PhotoService,
              public actionSheetController: ActionSheetController) {}


  public async showActionSheet(photo, position) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
        }
      }]
    });
    await actionSheet.present();
  }

}
